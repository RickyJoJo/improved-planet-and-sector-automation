package ipa.scriptedtriggers

import ipa.Policies
import ipa.Resources
import ipa.Variables
import ipa.blocks.triggers.CountryScriptedTrigger

class IpaMissingDesiredIncomeRareCrystals : CountryScriptedTrigger("ipa_missing_desired_income_rare_crystals") {

    init {
        addDesiredIncome(Resources.RareCrystals, Variables.IncomeStrategicDesired, Policies.incomeStepsStrategic)
    }

}
