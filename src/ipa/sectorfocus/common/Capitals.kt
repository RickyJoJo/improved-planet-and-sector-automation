package ipa.sectorfocus.common

import ipa.PlanetConstants
import ipa.blocks.sector.SectorFocus

fun SectorFocus.addCommonCapitals() {

    for (building in PlanetConstants.CapitalBuildings) {
        addBuilding(building, priorities.urgent) { }
    }

}
