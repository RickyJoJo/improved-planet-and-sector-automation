package ipa.scriptedtriggers

import ipa.Policies
import ipa.Resources
import ipa.Variables
import ipa.blocks.triggers.CountryScriptedTrigger

class IpaMissingIncomeFood : CountryScriptedTrigger("ipa_missing_income_food") {

    init {
        addMinimumIncome(Resources.Food, Variables.IncomeFoodMinimum, Policies.incomeSteps)
    }

}
