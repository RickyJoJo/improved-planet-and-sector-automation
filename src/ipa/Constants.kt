package ipa

object ModInfo {
    const val Version = "0.9.3"
    const val UpdatedAt = "2021-04-11"
}

object PlanetConstants {

    object Districts {
        val Housing = arrayOf(
            "district_city",
            "district_hab_housing",
            "district_hive",
            "district_nexus",
            "district_rw_city",
            "district_rw_hive",
            "district_rw_nexus"
        )
        val Mining = arrayOf(
            "district_mining",
            "district_hab_mining",
            "district_mining_uncapped"
        )
        val Generator = arrayOf(
            "district_generator",
            "district_rw_generator",
            "district_hab_energy",
            "district_generator_uncapped"
        )
        val Farming = arrayOf(
            "district_farming",
            "district_rw_farming",
            "district_farming_uncapped"
        )
        val Research = arrayOf(
            "district_rw_science",
            "district_hab_science"
        )
        val Leisure = arrayOf(
            "district_hab_cultural",
            "district_arcology_leisure"
        )
        val Commercial = arrayOf(
            "district_rw_commercial"
        )
        val All = Housing + Mining + Generator + Farming + Research + Leisure + Commercial
    }

    object Designations {
        val Capital = arrayOf(
            "col_capital",
            "col_capital_hive",
            "col_capital_machine"
        )
        val Generator = arrayOf(
            "col_generator",
            "col_habitat_energy",
            "col_ring_generator"
        )
        val Mining = arrayOf(
            "col_mining",
            "col_habitat_mining"
        )
        val Farming = arrayOf(
            "col_farming",
            "col_ring_farming"
        )
        val Research = arrayOf(
            "col_research",
            "col_ecu_research",
            "col_ring_research",
            "col_habitat_research"
        )
        val Leisure = arrayOf(
            "col_habitat_leisure"
        )
        val Commercial = arrayOf(
            "col_ring_trade"
        )
        val Urban = arrayOf(
            "col_city",
            "col_nexus",
            "col_hive"
        )
        val Factory = arrayOf(
            "col_factory",
            "col_mac_factory",
            "col_habitat_factory",
            "col_ecu_factory"
        )
        val Refinery = arrayOf(
            "col_refinery",
            "col_habitat_refinery"
        )
        val Forge = arrayOf(
            "col_foundry",
            "col_hiv_foundry",
            "col_mac_foundry",
            "col_habitat_foundry",
            "col_ecu_foundry"
        )
        val Fortress = arrayOf(
            "col_fortress",
            "col_habitat_fortress"
        )
        val Rural = arrayOf(
            "col_rural",
            "col_rural_gestalt"
        )
        val Penal = arrayOf(
            "col_penal"
        )
        val Resort = arrayOf(
            "col_resort"
        )
        val Mix = arrayOf(
            "col_ecu_mix",
            "col_ring_mix"
        )
        val Bureau = arrayOf(
            "col_bureau"
        )

        val All = Capital +
            Generator +
            Mining +
            Farming +
            Research +
            Leisure +
            Commercial +
            Urban +
            Factory +
            Refinery +
            Forge +
            Fortress +
            Rural +
            Penal +
            Resort +
            Mix +
            Bureau
    }

    val HousingBuildingsT1 = arrayOf("building_luxury_residence", "building_hive_warren", "building_drone_storage")
    // val HousingBuildingsT2 = arrayOf("building_luxury_residence", "building_hive_warren", "building_drone_storage")

    val AmenitiesBuildingsT1 = arrayOf("building_holo_theatres")
    val AmenitiesBuildingsT2 = arrayOf("building_hyper_entertainment_forum")

    val CapitalBuildings = arrayOf(
        "building_colony_shelter",
        "building_capital",
        "building_major_capital",
        "building_system_capital",
        "building_deployment_post",
        "building_machine_capital",
        "building_machine_major_capital",
        "building_machine_system_capital",
        "building_hive_capital",
        "building_hive_major_capital",
        "building_hab_capital",
        "building_hab_major_capital",
        "building_resort_capital",
        "building_slave_capital",
        "building_slave_major_capital"
    )

    val CrimeBuildingsT1 = arrayOf("building_precinct_house", "building_sentinel_posts")

    val AdminCapBuildingsT1 = arrayOf("building_bureaucratic_1", "building_uplink_node", "building_hive_node")
    val AdminCapBuildingsT2 = arrayOf("building_bureaucratic_2", "building_network_junction", "building_hive_cluster")
    val AdminCapBuildingsT3 = arrayOf("building_system_conflux", "building_hive_confluence")
}

object Resources {
    const val Minerals = "minerals"
    const val Energy = "energy"
    const val Food = "food"
    const val Alloys = "alloys"
    const val ConsumerGoods = "consumer_goods"
    const val RareCrystals = "rare_crystals"
    const val ExoticGases = "exotic_gases"
    const val VolatileMotes = "volatile_motes"
}

object Sectors {
    const val BuildMoreJobsLimit = 0
}

object Policies {
    val incomeSteps = arrayOf(
        0, 1, 5, 10, 20, 30, 50, 75, 100, 150, 200, 250, 500, 750, 1000,
        1500, 2000, 3000, 5000, 10000, 15000, 20000, 30000, 40000, 50000
    )
    val incomeStepsStrategic = arrayOf(0, 1, 3, 5, 10, 20, 30, 50, 75, 100, 150, 200, 250)
}

enum class Variables(val key: String) {
    Initialized("ipa_initialized"),

    IncomeEnergyMinimum("ipa_income_energy_minimum"),
    IncomeMineralsMinimum("ipa_income_minerals_minimum"),

    IncomeFoodMinimum("ipa_income_food_minimum"),
    IncomeFoodDesired("ipa_income_food_desired"),

    IncomeConsumerGoodsMinimum("ipa_income_consumer_goods_minimum"),
    IncomeConsumerGoodsDesired("ipa_income_consumer_goods_desired"),

    IncomeAlloysMinimum("ipa_income_alloys_minimum"),
    IncomeAlloysDesired("ipa_income_alloys_desired"),

    IncomeStrategicMinimum("ipa_income_strategic_minimum"),
    IncomeStrategicDesired("ipa_income_strategic_desired"),

    OptionsAllowUpgradeStronghold("ipa_options_allow_upgrade_strongholds"), // 0 = no, 1 = yes
    OptionsAllowAgriFarmBuildings("ipa_options_allow_agri_farm_buildings"), // 0 = no, 1 = yes
    OptionsForceShowIncomeOptions("ipa_options_force_show_income_options"), // 0 = no, 1 = yes

    OptionsAllowGrowthClinic("ipa_options_allow_growth_clinic"),
    OptionsAllowGrowthHospital("ipa_options_allow_growth_hospital"),
    OptionsAllowGrowthRobotAssembly("ipa_options_allow_growth_robot_assembly"),
    OptionsAllowGrowthCloneVats("ipa_options_allow_growth_clone_vats"),
    OptionsAllowGrowthMachineAssembly("ipa_options_allow_growth_machine_assembly"),
    OptionsAllowGrowthHiveSpawningPool("ipa_options_allow_growth_hive_spawning_pool"),
    OptionsAllowGrowthNecroidElevation("ipa_options_allow_growth_necroid_elevation"),

    OptionsUpgradeMinFreeSlots("ipa_options_upgrade_min_free_slots"), // num
    OptionsManageAdminCap("ipa_options_manage_admin_cap"), // 0 = enabled, 1 = byzantine, 2 = disabled
    OptionsDisallowUnity("ipa_options_disallow_unity"), // 0 = enabled, 1 = disabled
}
