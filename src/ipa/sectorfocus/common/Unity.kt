package ipa.sectorfocus.common

import ipa.blocks.sector.SectorFocus

fun SectorFocus.addCommonUnity() {
    addCommonUnityHive()
    addCommonUnityRegular()
    addCommonUnityMachine()
    addCommonUnitySpiritual()
}

fun SectorFocus.addCommonUnityHive() {

    addBuilding("building_hive_node", priorities.unity) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                addFoodIncomeBlock()
            }
            unityAllowed()
        }
    }

    addBuilding("building_hive_cluster", priorities.unity) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                addFoodIncomeBlock()
            }
            unityAllowed()
        }
    }

    addBuilding("building_hive_confluence", priorities.unity) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                missingIncomeRareCrystals = true
                addFoodIncomeBlock()
            }
            unityAllowed()
        }
    }

}

fun SectorFocus.addCommonUnityMachine() {

    addBuilding("building_uplink_node", priorities.unity) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
            }
            unityAllowed()
        }
    }

    addBuilding("building_network_junction", priorities.unity) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
            }
            unityAllowed()
        }
    }

    addBuilding("building_system_conflux", priorities.unity) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                missingIncomeRareCrystals = true
            }
            unityAllowed()
        }
    }

    addBuilding("building_alpha_hub", priorities.unity) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                missingIncomeRareCrystals = true
            }
            unityAllowed()
        }
    }

}

fun SectorFocus.addCommonUnityRegular() {

    addBuilding("building_autochthon_monument", priorities.unity) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                missingIncomeConsumerGoods = true
            }
            unityAllowed()
        }
    }

    addBuilding("building_heritage_site", priorities.unity) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                missingIncomeConsumerGoods = true
            }
            unityAllowed()
        }
    }

    addBuilding("building_hypercomms_forum", priorities.unity) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                missingIncomeConsumerGoods = true
                missingIncomeRareCrystals = true
            }
            unityAllowed()
        }
    }

    addBuilding("building_autocurating_vault", priorities.unity) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                missingIncomeConsumerGoods = true
                missingIncomeRareCrystals = true
            }
            unityAllowed()
        }
    }

}

fun SectorFocus.addCommonUnitySpiritual() {

    addBuilding("building_temple", priorities.unity) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
            }
            unityAllowed()
        }
    }

    addBuilding("building_holotemple", priorities.unity) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
            }
            unityAllowed()
        }
    }

    addBuilding("building_sacred_nexus", priorities.unity) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                missingIncomeRareCrystals = true
            }
            unityAllowed()
        }
    }

    addBuilding("building_citadel_of_faith", priorities.unity) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                missingIncomeRareCrystals = true
            }
            unityAllowed()
        }
    }

}
