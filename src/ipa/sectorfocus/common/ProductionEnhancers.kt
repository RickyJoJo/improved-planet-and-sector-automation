package ipa.sectorfocus.common

import ipa.PlanetConstants
import ipa.blocks.logic.Comparators
import ipa.blocks.sector.SectorFocus

fun SectorFocus.addCommonProductionEnhancers(
    onlyOnMatchingDesignation: Boolean = true,
    numMatchingDistricts: Int = 1
) {

    addBuilding("building_mineral_purification_plant", priorities.enhancers) {
        modifierOR(priorities.disable) {
            disableIfNoNeedForJobs()
            NOR {
                comment = "Disallow if not enough districts build"
                for (district in PlanetConstants.Districts.Mining) {
                    numDistricts(district, Comparators.GTEQ, numMatchingDistricts)
                }
            }
            if (onlyOnMatchingDesignation) {
                NOR {
                    comment = "Disallow on invalid types"
                    hasDesignationMining()
                }
            }
        }
    }
    addBuilding("building_mineral_purification_hub", priorities.enhancers + 1) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeVolatileMotes = true
            }
        }
    }

    addBuilding("building_energy_grid", priorities.enhancers) {
        modifierOR(priorities.disable) {
            disableIfNoNeedForJobs()
            NOR {
                comment = "Disallow if not enough districts build"
                for (district in PlanetConstants.Districts.Generator) {
                    numDistricts(district, Comparators.GTEQ, numMatchingDistricts)
                }
            }
            if (onlyOnMatchingDesignation) {
                NOR {
                    comment = "Disallow on invalid types"
                    hasDesignationGenerator()
                }
            }
        }
    }

    addBuilding("building_energy_nexus", priorities.enhancers + 1) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeExoticGases = true
            }
        }
    }

    addBuilding("building_food_processing_facility", priorities.enhancers) {
        modifierOR(priorities.disable) {
            disableIfNoNeedForJobs()
            owner {
                usesFood = false
            }
            NOR {
                comment = "Disallow if not enough districts build"
                for (district in PlanetConstants.Districts.Farming) {
                    numDistricts(district, Comparators.GTEQ, numMatchingDistricts)
                }
            }
            if (onlyOnMatchingDesignation) {
                NOR {
                    comment = "Disallow on invalid types"
                    hasDesignationFarming()
                }
            }
        }
    }

    addBuilding("building_food_processing_center", priorities.enhancers + 1) {
        modifierOR(priorities.disable) {
            owner {
                usesFood = false
            }
            controller {
                missingIncomeVolatileMotes = true
            }
        }
    }

}
