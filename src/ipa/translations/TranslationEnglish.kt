package ipa.translations

import ipa.ModInfo
import ipa.Policies
import ipa.blocks.TranslationFile

class TranslationEnglish : TranslationFile("localisation/english/ipa_l_english.yml") {

    init {
        language = "l_english"
        addBase(this)
    }

    companion object {

        fun addBase(tx: TranslationFile) {
            tx.addVarious()
            tx.addMenuOptions()
            tx.addMenuIncomes()
        }

        fun TranslationFile.addMenuIncomes() {
            addMenuIncomeTranslation(
                "energy_minimum",
                "£energy£ §BMinimum §!Energy Income",
                Policies.incomeSteps
            )
            addMenuIncomeTranslation(
                "minerals_minimum",
                "£minerals£ §BMinimum §!Minerals Income",
                Policies.incomeSteps
            )
            addMenuIncomeTranslation(
                "food_minimum",
                "£food£ §BMinimum §!Food Income",
                Policies.incomeSteps
            )
            addMenuIncomeTranslation(
                "food_desired",
                "£food£ §EDesired §!Food Income",
                Policies.incomeSteps
            )
            addMenuIncomeTranslation(
                "alloys_minimum",
                "£alloys£ §BMinimum §!Alloys Income",
                Policies.incomeSteps
            )
            addMenuIncomeTranslation(
                "alloys_desired",
                "£alloys£ §EDesired §!Alloys Income",
                Policies.incomeSteps
            )
            addMenuIncomeTranslation(
                "consumer_goods_minimum",
                "£consumer_goods£ §BMinimum §!Consumer Goods Income",
                Policies.incomeSteps
            )
            addMenuIncomeTranslation(
                "consumer_goods_desired",
                "£consumer_goods£ §EDesired §!Consumer Goods Income",
                Policies.incomeSteps
            )
            addMenuIncomeTranslation(
                "strategic_minimum",
                "£volatile_motes£ £exotic_gases£ £rare_crystals£ §BMinimum §!Strategic Income",
                Policies.incomeStepsStrategic
            )
            addMenuIncomeTranslation(
                "strategic_desired",
                "£volatile_motes£ £exotic_gases£ £rare_crystals£ §EDesired §!Strategic Income",
                Policies.incomeStepsStrategic
            )
        }

        fun TranslationFile.addMenuIncomeTranslation(eventKey: String, dialogLabel: String, steps: Array<Int>) {

            // Main Menu
            add("ipa_settings_main.1.$eventKey.disabled", "$dialogLabel: §RDisabled")
            for (step in steps) {
                add("ipa_settings_main.1.$eventKey.step$step", "$dialogLabel: §G$step")
            }
            add("ipa_settings_main.1.$eventKey.unlimited", "$dialogLabel: §RUnlimited")

            // Sub Menu
            add("ipa_settings_$eventKey.1.title", dialogLabel)
            add("ipa_settings_$eventKey.1.desc", "")
            add("ipa_settings_$eventKey.1.disabled", "$dialogLabel: §RDisabled")
            add("ipa_settings_$eventKey.1.unlimited", "$dialogLabel: §RUnlimited")
            for (step in steps) {
                add("ipa_settings_$eventKey.1.step$step", "$dialogLabel: §G$step")
            }
            add("ipa_settings_$eventKey.1.back", "§RBack")
        }

        fun TranslationFile.addVarious() {
            // Sector Types
            add("st_designated", "Designated Focus")
            add("st_designated_desc", "The Sector will focus on planet designations and develop them accordingly.")
            add("st_mixed", "Mixed Focus")
            add(
                "st_mixed_desc",
                "The Sector will build on all planets, regardless of planet designations. This is inherently less efficient due to the loss of specialization bonuses but may be advantageous in the early game."
            )
            add("st_strict", "Strict Focus")
            add(
                "st_strict_desc",
                "The Sector will focus on planet designations but will never cross-build mining/farming/generator districts. If a planet is designated as a mining planet it will not build generator or farming districts."
            )
            add("st_designated_wip", "Designated Focus (WIP)")
            add(
                "st_designated_wip_desc",
                "Includes work-in-progress features and changes that may not be fully ready yet. If this designation is causing problems please report them and consider switching to regular designated focus."
            )
        }

        fun TranslationFile.addMenuOptions() {
            // Edicts
            add("edict_ipa_settings_menu", "§BImproved Planet & Sector Automation")


            // Settings Menu
            add("ipa_settings_main.1.title", "Improved Planet and Sector Automation: §Bv${ModInfo.Version}")
            add(
                "ipa_settings_main.1.desc",
                "Improved Planet and Sector Automation: §Bv${ModInfo.Version} §!Updated: ${ModInfo.UpdatedAt}"
            )
            add("ipa_settings_main.1.options", "§BOptions")
            add("ipa_settings_main.1.close", "§RClose")
            add("ipa_settings_options.1.title", "IPA: §BOptions")
            add("ipa_settings_options.1.desc", "")
            add("ipa_settings_options.1.back", "§RBack")
            add("ipa_settings_options.1.allow_growth_buildings", "Configure: Growth buildings")
            add("ipa_settings_options.1.upgrade_min_free_slots", "Configure: Building upgrade min free slots")
            add("ipa_settings_options.1.admin_cap", "Configure: Admin cap buildings")
            add("ipa_settings_options.1.ipa_options_allow_upgrade_strongholds_yes", "Upgrade Strongholds: §GYes")
            add("ipa_settings_options.1.ipa_options_allow_upgrade_strongholds_no", "Upgrade Strongholds: §RNo")
            add(
                "ipa_settings_options.1.ipa_options_allow_agri_farm_buildings_yes",
                "Farm Building on Agri Designation: §GYes"
            )
            add(
                "ipa_settings_options.1.ipa_options_allow_agri_farm_buildings_no",
                "Farm Building on Agri Designation: §RNo"
            )
            add("ipa_settings_options.1.ipa_options_force_show_income_options_yes", "Force Show Income Options: §GYes")
            add("ipa_settings_options.1.ipa_options_force_show_income_options_no", "Force Show Income Options: §RNo")
            add("ipa_settings_options.1.ipa_options_disallow_unity_yes", "Build unity buildings: §GYes")
            add("ipa_settings_options.1.ipa_options_disallow_unity_no", "Build unity buildings: §RNo")


            // Settings Submenu: growth buildings
            add("ipa_settings_options_allow_growth_buildings.1.title", "IPA: §BAllow growth buildings")
            add("ipa_settings_options_allow_growth_buildings.1.desc", "")
            add("ipa_settings_options_allow_growth_buildings.1.clinic_allowed", "§GAllow §BOrganic Clinic")
            add("ipa_settings_options_allow_growth_buildings.1.clinic_forbidden", "§RForbid §BOrganic Clinic")
            add("ipa_settings_options_allow_growth_buildings.1.hospital_allowed", "§GAllow §BHospital")
            add("ipa_settings_options_allow_growth_buildings.1.hospital_forbidden", "§RForbid §BHospital")
            add("ipa_settings_options_allow_growth_buildings.1.robot_assembly_allowed", "§GAllow §BRobot Assembly")
            add("ipa_settings_options_allow_growth_buildings.1.robot_assembly_forbidden", "§RForbid §BRobot Assembly")
            add("ipa_settings_options_allow_growth_buildings.1.clone_vats_allowed", "§GAllow §BClone Vats")
            add("ipa_settings_options_allow_growth_buildings.1.clone_vats_forbidden", "§RForbid §BClone Vats")
            add(
                "ipa_settings_options_allow_growth_buildings.1.machine_assembly_allowed",
                "§GAllow §BMachine Assembly Planet"
            )
            add(
                "ipa_settings_options_allow_growth_buildings.1.machine_assembly_forbidden",
                "§RForbid §BMachine Assembly Planet"
            )
            add(
                "ipa_settings_options_allow_growth_buildings.1.hive_spawning_pool_allowed",
                "§GAllow §BHive Spawning Pool"
            )
            add(
                "ipa_settings_options_allow_growth_buildings.1.hive_spawning_pool_forbidden",
                "§RForbid §BHive Spawning Pool"
            )
            add(
                "ipa_settings_options_allow_growth_buildings.1.necroid_elevation_allowed",
                "§GAllow §BNecroid Elevation"
            )
            add(
                "ipa_settings_options_allow_growth_buildings.1.necroid_elevation_forbidden",
                "§RForbid §BNecroid Elevation"
            )
            add("ipa_settings_options_allow_growth_buildings.1.back", "§RBack")


            // Settings Submenu: settings upgrade min free slots
            add(
                "ipa_settings_options_upgrade_min_free_slots.1.title",
                "IPA: §BBuilding upgrade min free slots"
            )
            add(
                "ipa_settings_options_upgrade_min_free_slots.1.desc",
                "Require a specified amount of free slots before upgrading production buildings (factory, foundry, research labs). This conserves strategic resources while there is no need to expend them. Buildings will always be upgraded if there are no more free slots available on the planet and more jobs are required."
            )
            for (i in 0..10) {
                add("ipa_settings_options_upgrade_min_free_slots.1.option$i.current", "§GRequire $i free slots")
                add("ipa_settings_options_upgrade_min_free_slots.1.option$i.change", "§BRequire $i free slots")
            }
            add("ipa_settings_options_upgrade_min_free_slots.1.back", "§RBack")


            // Settings Submenu: admin cap
            add("ipa_settings_options_admin_cap.1.title", "IPA: §BAdmin Cap Options")
            add("ipa_settings_options_admin_cap.1.desc", "Set admin cap building rules.")

            add("ipa_settings_options_admin_cap.1.option_enabled.current", "§GEnabled admin cap buildings")
            add("ipa_settings_options_admin_cap.1.option_enabled.change", "§BEnable admin cap buildings")

            add("ipa_settings_options_admin_cap.1.option_designation.current", "§GEnabled only on designated colonies")
            add("ipa_settings_options_admin_cap.1.option_designation.change", "§BEnable only on designated colonies")

            add("ipa_settings_options_admin_cap.1.option_disabled.current", "§GDisabled admin cap buildings")
            add("ipa_settings_options_admin_cap.1.option_disabled.change", "§BDisable admin cap buildings")

            add("ipa_settings_options_admin_cap.1.back", "§RBack")

        }
    }

}
