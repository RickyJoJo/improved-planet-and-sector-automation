package ipa.scriptedtriggers

import ipa.Policies
import ipa.Resources
import ipa.Variables
import ipa.blocks.triggers.CountryScriptedTrigger

class IpaMissingIncomeConsumerGoods : CountryScriptedTrigger("ipa_missing_income_consumer_goods") {

    init {
        addMinimumIncome(Resources.ConsumerGoods, Variables.IncomeConsumerGoodsMinimum, Policies.incomeSteps)
    }

}
