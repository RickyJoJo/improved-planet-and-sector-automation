package ipa.scriptedtriggers

import ipa.Policies
import ipa.Resources
import ipa.Variables
import ipa.blocks.triggers.CountryScriptedTrigger

class IpaMissingDesiredIncomeConsumerGoods : CountryScriptedTrigger("ipa_missing_desired_income_consumer_goods") {

    init {
        addDesiredIncome(Resources.ConsumerGoods, Variables.IncomeConsumerGoodsDesired, Policies.incomeSteps)
    }

}
