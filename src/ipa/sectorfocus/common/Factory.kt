package ipa.sectorfocus.common

import ipa.blocks.sector.SectorFocus

fun SectorFocus.addCommonFactory() {

    addBuilding("building_factory_1", priorities.consumerGoods) {

        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingIncomeConsumerGoods = true
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            hasDesignationResearch()
            hasDesignationRefinery()
            hasDesignationForge()
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
                missingIncomeMinerals = true
                missingDesiredIncomeConsumerGoods = false
            }
            owner {
                usesConsumerGoods = false
            }
        }
    }

    addBuilding("building_factory_2", priorities.consumerGoods + 1.0) {

        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingIncomeConsumerGoods = true
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            hasDesignationResearch()
            hasDesignationRefinery()
            hasDesignationForge()
            disableIfNoNeedForJobs()
            upgradeMinFreeBuildingSlots()
            controller {
                missingIncomeEnergy = true
                missingIncomeMinerals = true
                missingIncomeRareCrystals = true
                missingDesiredIncomeConsumerGoods = false
            }
            owner {
                usesConsumerGoods = false
            }
        }
    }

    addBuilding("building_factory_3", priorities.consumerGoods + 2.0) {

        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingIncomeConsumerGoods = true
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            hasDesignationResearch()
            hasDesignationRefinery()
            hasDesignationForge()
            disableIfNoNeedForJobs()
            upgradeMinFreeBuildingSlots()
            controller {
                missingIncomeEnergy = true
                missingIncomeMinerals = true
                missingIncomeRareCrystals = true
                missingDesiredIncomeConsumerGoods = false
            }
            owner {
                usesConsumerGoods = false
            }
        }
    }

}
