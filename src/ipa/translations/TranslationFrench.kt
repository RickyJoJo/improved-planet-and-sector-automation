package ipa.translations

import ipa.blocks.TranslationFile

class TranslationFrench : TranslationFile("localisation/french/ipa_l_french.yml") {

    init {
        language = "l_french"
        TranslationEnglish.addBase(this)
    }

}
