package ipa.blocks

open class TranslationFile(target: String, cb: (TranslationFile.() -> Unit)? = null) :
    AbstractFile<TemplateRender>(target) {

    var language: String? = "l_english"
    val translations: MutableMap<String, String> = mutableMapOf()

    init {
        addBOM = true
        if (cb != null) cb()
    }

    fun add(key: String, value: String) {
        translations[key] = value
    }

    override fun newTemplateRender(): TemplateRender {
        return TemplateRender("    ")
    }

    override fun writeContent(render: TemplateRender) {
        if (language != null) {
            render.add("$language:")
            render.indent++
        }

        for ((key, value) in translations) {
            render.add("$key: \"$value\"")
        }

        if (language != null) {
            render.indent--
        }
    }

}
