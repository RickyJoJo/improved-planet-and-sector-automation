package ipa.scriptedtriggers

import ipa.Policies
import ipa.Resources
import ipa.Variables
import ipa.blocks.triggers.CountryScriptedTrigger

class IpaMissingIncomeRareCrystals : CountryScriptedTrigger("ipa_missing_income_rare_crystals") {

    init {
        addMinimumIncome(Resources.RareCrystals, Variables.IncomeStrategicMinimum, Policies.incomeStepsStrategic)
    }

}
