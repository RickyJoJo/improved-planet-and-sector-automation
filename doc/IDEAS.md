# Work in progress ideas, brainstorm, etc

## TODO
- add dedicated unity worlds designation
- add commerce world designation (similar to habitats)
- add betharian power plant support
- build temples over holo-theatres if available
- add building_machine_assembly_complex ?
- Add increased priority for resource income < 0 (not just < threshold to ensure they are preferred over other resources below threshold!)
- support for Noble Estates
- planetary decision to disable any automation on that world
- improve habitat support
- improve ringworlds support
- update base focuses from basegame or just remove (test if AI is affected!)
