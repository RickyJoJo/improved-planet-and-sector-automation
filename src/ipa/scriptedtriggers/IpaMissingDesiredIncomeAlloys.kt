package ipa.scriptedtriggers

import ipa.Policies
import ipa.Resources
import ipa.Variables
import ipa.blocks.triggers.CountryScriptedTrigger

class IpaMissingDesiredIncomeAlloys : CountryScriptedTrigger("ipa_missing_desired_income_alloys") {

    init {
        addDesiredIncome(Resources.Alloys, Variables.IncomeAlloysDesired, Policies.incomeSteps)
    }

}
