package ipa.translations

import ipa.blocks.TranslationFile

class TranslationGerman : TranslationFile("localisation/german/ipa_l_german.yml") {

    init {
        language = "l_german"
        TranslationEnglish.addBase(this)
    }

}
