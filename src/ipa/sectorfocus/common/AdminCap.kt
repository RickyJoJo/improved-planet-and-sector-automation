package ipa.sectorfocus.common

import ipa.PlanetConstants
import ipa.blocks.logic.Comparators
import ipa.blocks.sector.SectorFocus

fun SectorFocus.addCommonAdminCap() {

    for (building in PlanetConstants.AdminCapBuildingsT1) {
        addBuilding(building, priorities.production) {
            modifierOR(priorities.disable) {
                // AND {
                //    comment = "Disallow on non-bureau worlds if owner does not have civic_byzantine_bureaucracy"
                //    NOT {
                //        owner { hasValidCivic("civic_byzantine_bureaucracy") }
                //    }
                //    hasDesignationBureau(false)
                // }
                owner {
                    comment = "Disallow if empire is within administrative cap"
                    empireSprawlCapOver(Comparators.LTEQ, 0)
                }
                disableIfNoNeedForJobs()
                adminCapAllowed(false)
                controller {
                    missingIncomeEnergy = true
                    addAdminCapRequirementsGestaltAltBlock()
                }
            }
        }
    }

    for (building in PlanetConstants.AdminCapBuildingsT2)
        addBuilding(building, priorities.production + 1) {
            modifierOR(priorities.disable) {
                owner {
                    comment = "Disallow if empire is within administrative cap"
                    empireSprawlCapOver(Comparators.LTEQ, 0)
                }
                disableIfNoNeedForJobs()
                adminCapAllowed(false)
                upgradeMinFreeBuildingSlots()
                controller {
                    missingIncomeEnergy = true
                    addAdminCapRequirementsGestaltAltBlock()
                }
            }
        }

    for (building in PlanetConstants.AdminCapBuildingsT3)
        addBuilding(building, priorities.production + 2) {
            modifierOR(priorities.disable) {
                owner {
                    comment = "Disallow if empire is within administrative cap"
                    empireSprawlCapOver(Comparators.LTEQ, 0)
                }
                disableIfNoNeedForJobs()
                adminCapAllowed(false)
                upgradeMinFreeBuildingSlots()
                controller {
                    missingIncomeEnergy = true
                    addAdminCapRequirementsGestaltAltBlock()
                }
            }
        }

}
