package ipa.sectorfocus.common

import ipa.blocks.sector.SectorFocus

fun SectorFocus.addCommonFoundry() {

    addBuilding("building_foundry_1", priorities.alloys) {

        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingIncomeAlloys = true
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            hasDesignationResearch()
            hasDesignationRefinery()
            hasDesignationFactory()
            hasDesignationLeisure()
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
                missingIncomeMinerals = true
                missingDesiredIncomeAlloys = false
            }
        }
    }

    addBuilding("building_foundry_2", priorities.alloys + 1.0) {

        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingIncomeAlloys = true
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            hasDesignationResearch()
            hasDesignationRefinery()
            hasDesignationFactory()
            hasDesignationLeisure()
            disableIfNoNeedForJobs()
            upgradeMinFreeBuildingSlots()
            controller {
                missingIncomeEnergy = true
                missingIncomeMinerals = true
                missingIncomeVolatileMotes = true
                missingDesiredIncomeAlloys = false
            }
        }
    }

    addBuilding("building_foundry_3", priorities.alloys + 1.0) {

        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingIncomeAlloys = true
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            hasDesignationResearch()
            hasDesignationRefinery()
            hasDesignationFactory()
            hasDesignationLeisure()
            disableIfNoNeedForJobs()
            upgradeMinFreeBuildingSlots()
            controller {
                missingIncomeEnergy = true
                missingIncomeMinerals = true
                missingIncomeVolatileMotes = true
                missingDesiredIncomeAlloys = false
            }
        }
    }

}
