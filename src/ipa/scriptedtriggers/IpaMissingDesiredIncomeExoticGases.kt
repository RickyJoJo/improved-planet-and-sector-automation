package ipa.scriptedtriggers

import ipa.Policies
import ipa.Resources
import ipa.Variables
import ipa.blocks.triggers.CountryScriptedTrigger

class IpaMissingDesiredIncomeExoticGases : CountryScriptedTrigger("ipa_missing_desired_income_exotic_gases") {

    init {
        addDesiredIncome(Resources.ExoticGases, Variables.IncomeStrategicDesired, Policies.incomeStepsStrategic)
    }

}
