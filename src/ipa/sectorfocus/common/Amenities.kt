package ipa.sectorfocus.common


import ipa.PlanetConstants
import ipa.blocks.logic.Comparators
import ipa.blocks.sector.SectorFocus

fun SectorFocus.addCommonAmenities() {

    // amenities T1
    for (building in PlanetConstants.AmenitiesBuildingsT1) {
        addBuilding(building, 1.0) {

            modifierAND(priorities.prebuild) {
                comment = "Pre-build when approaching threshold and not to many free jobs"
                freeAmenities(Comparators.LT, 1)
                freeJobs(Comparators.LT, 5)
            }

            modifier(priorities.urgent / priorities.prebuild) {
                comment = "Increase priority when out of amenities"
                freeAmenities(Comparators.LT, 0)
            }

            modifier(priorities.crisis / priorities.urgent) {
                comment = "Increase to crisis status if this gets out of hand"
                freeAmenities(Comparators.LT, -10)
            }

            modifierOR(priorities.disable) {
                comment = "Disallow when no workers or available or free amenities are present"
                freeJobs(Comparators.GT, 2)
                freeAmenities(Comparators.GT, 0)
            }
        }
    }

    // amenities T2
    addBuilding("building_hyper_entertainment_forum", 1.0) {

        // Prebuild+1 because its tier 2
        modifierAND(priorities.prebuild + 1) {
            comment = "Pre-build when approaching threshold and not to many free jobs"
            freeAmenities(Comparators.LT, 1)
            freeJobs(Comparators.LT, 5)
        }

        modifier(priorities.urgent / priorities.prebuild) {
            comment = "Increase priority when out of amenities"
            freeAmenities(Comparators.LT, 0)
        }

        modifier(priorities.crisis / priorities.urgent) {
            comment = "Increase to crisis status if this gets out of hand"
            freeAmenities(Comparators.LT, -10)
        }

        modifierOR(priorities.disable) {
            comment = "Disallow when no workers or available"
            freeJobs(Comparators.GT, 2)
            freeAmenities(Comparators.GT, 0)
        }
    }

}
