package ipa.scriptedtriggers

import ipa.Policies
import ipa.Resources
import ipa.Variables
import ipa.blocks.triggers.CountryScriptedTrigger

class IpaMissingIncomeAlloys : CountryScriptedTrigger("ipa_missing_income_alloys") {

    init {
        addMinimumIncome(Resources.Alloys, Variables.IncomeAlloysMinimum, Policies.incomeSteps)
    }

}
