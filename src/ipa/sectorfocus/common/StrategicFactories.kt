package ipa.sectorfocus.common

import ipa.blocks.sector.SectorFocus

fun SectorFocus.addCommonStrategicFactories() {

    addBuilding("building_chemical_plant", priorities.strategic) {
        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingIncomeVolatileMotes = true
            }
        }
        modifierOR(priorities.disable) {
            comment = "Disallow if to many free jobs or above desired income"
            disableIfNoNeedForJobs()
            hasDesignationResearch()
            controller {
                missingDesiredIncomeVolatileMotes = false
            }
        }
    }

    addBuilding("building_refinery", priorities.strategic) {
        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingIncomeExoticGases = true
            }
        }
        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            disableIfNoNeedForJobs()
            hasDesignationResearch()
            controller {
                missingDesiredIncomeExoticGases = false
            }
        }
    }

    addBuilding("building_crystal_plant", priorities.strategic) {
        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingIncomeRareCrystals = true
            }
        }
        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            disableIfNoNeedForJobs()
            hasDesignationResearch()
            controller {
                missingDesiredIncomeRareCrystals = false
            }
        }
    }

}
