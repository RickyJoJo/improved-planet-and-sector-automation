package ipa.blocks.sector

import ipa.blocks.TemplateBlock
import ipa.blocks.TemplateRender

abstract class SectorConstruct(val constructType: String, val constructKey: String) : TemplateBlock(constructType) {

    val weight = SectorWeight(this)

    init {
        comment = "$constructType: $constructKey"
        children.add(weight)
    }

    override fun renderBeforeChildren(render: TemplateRender) {
        render.add("key = $constructKey")
    }

    fun weight(weight: Double, block: (SectorWeight.() -> Unit)) {
        this.weight.baseWeight = weight
        this.weight.block()
    }

}
