package ipa.scriptedtriggers

import ipa.Policies
import ipa.Resources
import ipa.Variables
import ipa.blocks.triggers.CountryScriptedTrigger

class IpaMissingDesiredIncomeVolatileMotes : CountryScriptedTrigger("ipa_missing_desired_income_volatile_motes") {

    init {
        addDesiredIncome(Resources.VolatileMotes, Variables.IncomeStrategicDesired, Policies.incomeStepsStrategic)
    }

}
