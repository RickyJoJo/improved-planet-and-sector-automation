package ipa.sectorfocus.common

import ipa.blocks.sector.SectorFocus

fun SectorFocus.addCommonGrowth() {

    // hive
    addBuilding("building_spawning_pool", priorities.population) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                addFoodIncomeBlock()
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
            hasModifier("planet_population_control_gestalt")
        }
    }

    // robot non-gestalt
    addBuilding("building_robot_assembly_plant", priorities.population) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                missingIncomeMinerals = true
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
            hasModifier("planet_robot_assembly_control")
            hasModifier("planet_population_control_gestalt")
        }
    }

    // machine gestalt
    addBuilding("building_machine_assembly_plant", priorities.population) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                missingIncomeAlloys = true
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
            hasModifier("planet_robot_assembly_control")
            hasModifier("planet_population_control_gestalt")
        }
    }
    addBuilding("building_machine_assembly_complex", priorities.population + 1) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                missingIncomeAlloys = true
                missingIncomeRareCrystals = true
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
            hasModifier("planet_robot_assembly_control")
            hasModifier("planet_population_control_gestalt")
        }
    }

    // bio
    addBuilding("building_clinic", priorities.population) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
            hasModifier("planet_population_control")
            hasModifier("planet_population_control_gestalt")
        }
    }
    addBuilding("building_hospital", priorities.population) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                missingIncomeExoticGases = true
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
            hasModifier("planet_population_control")
        }
    }
    addBuilding("building_clone_vats", priorities.population) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
            hasModifier("planet_population_control")
        }
    }

    // necroids
    addBuilding("building_necrophage_elevation_chamber", priorities.population) {
        modifierOR(priorities.disable) {
            controller {
                addFoodIncomeBlock()
                missingIncomeEnergy = true
                missingIncomeFood = true
                missingIncomeConsumerGoods = true
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
        }
    }
    addBuilding("building_necrophage_house_of_apotheosis", priorities.population + 1) {
        modifierOR(priorities.disable) {
            controller {
                addFoodIncomeBlock()
                missingIncomeEnergy = true
                missingIncomeFood = true
                missingIncomeConsumerGoods = true
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
        }
    }

}
