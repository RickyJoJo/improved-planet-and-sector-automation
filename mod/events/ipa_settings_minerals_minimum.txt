namespace = ipa_settings_minerals_minimum

# CountryEvent: ipa_settings_minerals_minimum.1
country_event = {
	id = ipa_settings_minerals_minimum.1
	title = ipa_settings_minerals_minimum.1.title
	desc = ipa_settings_minerals_minimum.1.desc
	hide_window = no
	is_triggered_only = yes

	# EventOption: ipa_settings_minerals_minimum.1.disabled
	option = {
		name = ipa_settings_minerals_minimum.1.disabled

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = -10000 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step0
	option = {
		name = ipa_settings_minerals_minimum.1.step0

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 0 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step1
	option = {
		name = ipa_settings_minerals_minimum.1.step1

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value <= 75 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 1 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step5
	option = {
		name = ipa_settings_minerals_minimum.1.step5

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value <= 100 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 5 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step10
	option = {
		name = ipa_settings_minerals_minimum.1.step10

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value <= 150 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 10 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step20
	option = {
		name = ipa_settings_minerals_minimum.1.step20

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value <= 200 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 20 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step30
	option = {
		name = ipa_settings_minerals_minimum.1.step30

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value <= 250 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 30 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step50
	option = {
		name = ipa_settings_minerals_minimum.1.step50

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value >= 0 }
			check_variable = { which = ipa_income_minerals_minimum value <= 500 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 50 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step75
	option = {
		name = ipa_settings_minerals_minimum.1.step75

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value >= 1 }
			check_variable = { which = ipa_income_minerals_minimum value <= 750 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 75 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step100
	option = {
		name = ipa_settings_minerals_minimum.1.step100

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value >= 5 }
			check_variable = { which = ipa_income_minerals_minimum value <= 1000 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 100 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step150
	option = {
		name = ipa_settings_minerals_minimum.1.step150

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value >= 10 }
			check_variable = { which = ipa_income_minerals_minimum value <= 1500 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 150 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step200
	option = {
		name = ipa_settings_minerals_minimum.1.step200

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value >= 20 }
			check_variable = { which = ipa_income_minerals_minimum value <= 2000 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 200 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step250
	option = {
		name = ipa_settings_minerals_minimum.1.step250

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value >= 30 }
			check_variable = { which = ipa_income_minerals_minimum value <= 3000 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 250 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step500
	option = {
		name = ipa_settings_minerals_minimum.1.step500

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value >= 50 }
			check_variable = { which = ipa_income_minerals_minimum value <= 5000 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 500 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step750
	option = {
		name = ipa_settings_minerals_minimum.1.step750

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value >= 75 }
			check_variable = { which = ipa_income_minerals_minimum value <= 10000 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 750 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step1000
	option = {
		name = ipa_settings_minerals_minimum.1.step1000

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value >= 100 }
			check_variable = { which = ipa_income_minerals_minimum value <= 15000 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 1000 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step1500
	option = {
		name = ipa_settings_minerals_minimum.1.step1500

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value >= 150 }
			check_variable = { which = ipa_income_minerals_minimum value <= 20000 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 1500 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step2000
	option = {
		name = ipa_settings_minerals_minimum.1.step2000

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value >= 200 }
			check_variable = { which = ipa_income_minerals_minimum value <= 30000 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 2000 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step3000
	option = {
		name = ipa_settings_minerals_minimum.1.step3000

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value >= 250 }
			check_variable = { which = ipa_income_minerals_minimum value <= 40000 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 3000 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step5000
	option = {
		name = ipa_settings_minerals_minimum.1.step5000

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value >= 500 }
			check_variable = { which = ipa_income_minerals_minimum value <= 50000 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 5000 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step10000
	option = {
		name = ipa_settings_minerals_minimum.1.step10000

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value >= 750 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 10000 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step15000
	option = {
		name = ipa_settings_minerals_minimum.1.step15000

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value >= 1000 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 15000 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step20000
	option = {
		name = ipa_settings_minerals_minimum.1.step20000

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value >= 1500 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 20000 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step30000
	option = {
		name = ipa_settings_minerals_minimum.1.step30000

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value >= 2000 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 30000 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step40000
	option = {
		name = ipa_settings_minerals_minimum.1.step40000

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value >= 3000 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 40000 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.step50000
	option = {
		name = ipa_settings_minerals_minimum.1.step50000

		trigger = {
			check_variable = { which = ipa_income_minerals_minimum value >= 5000 }
		}

		hidden_effect = {
			set_variable = { which = ipa_income_minerals_minimum value = 50000 }
			country_event = { id = ipa_settings_main.1 }
		}

	}

	# EventOption: ipa_settings_minerals_minimum.1.back
	option = {
		name = ipa_settings_minerals_minimum.1.back

		hidden_effect = {
			country_event = { id = ipa_settings_main.1 }
		}

	}

}

