package ipa.sectorfocus.common

import ipa.PlanetConstants
import ipa.blocks.sector.SectorFocus

fun SectorFocus.addCommonResourceDistricts() {

    for (district in PlanetConstants.Districts.Generator) {
        addDistrict(district, priorities.districts) {

            modifier(1.02) {
                comment = "increase priority slightly if below desired resource income"
                controller {
                    missingIncomeEnergy = true
                }
            }

            modifier(1.10) {
                comment = "increase priority for planets with matching focus" +
                    ", slightly stronger than missing income to ensure districts are build by designations first"
                hasDesignationGenerator()
            }

            modifierOR(priorities.disable) {
                comment = "Disallow for various conditions"
                disableIfNoNeedForJobs()
            }
        }
    }

    for (district in PlanetConstants.Districts.Farming) {
        addDistrict(district, priorities.districts) {

            modifier(1.02) {
                comment = "increase priority slightly if below desired resource income"
                controller {
                    missingIncomeFood = true
                }
            }

            modifier(1.10) {
                comment = "increase priority for planets with matching focus" +
                    ", slightly stronger than missing income to ensure districts are build by designations first"
                hasDesignationFarming()
            }

            modifierOR(priorities.disable) {
                comment = "Disallow for various conditions"
                disableIfNoNeedForJobs()
                controller {
                    missingDesiredIncomeFood = false
                }
                owner {
                    usesFood = false
                }
            }
        }
    }

    for (district in PlanetConstants.Districts.Mining) {
        addDistrict(district, priorities.districts) {

            modifier(1.02) {
                comment = "increase priority slightly if below desired resource income"
                controller {
                    missingIncomeMinerals = true
                }
            }

            modifier(1.10) {
                comment = "increase priority for planets with matching focus" +
                    ", slightly stronger than missing income to ensure districts are build by designations first"
                hasDesignationMining()
            }

            modifierOR(priorities.disable) {
                comment = "Disallow for various conditions"
                disableIfNoNeedForJobs()
            }
        }
    }

    for (district in PlanetConstants.Districts.Research) {
        addDistrict(district, priorities.districts) {

            modifier(1.10) {
                comment = "increase priority for planets with matching focus"
                hasDesignationResearch()
            }

            modifierOR(priorities.disable) {
                comment = "Disallow for various conditions"
                disableIfNoNeedForJobs()
                controller {
                    missingIncomeEnergy = true
                    missingIncomeMinerals = true
                }
            }
        }
    }

    for (district in PlanetConstants.Districts.Leisure) {
        addDistrict(district, priorities.districts) {

            modifier(1.10) {
                comment = "increase priority for planets with matching focus"
                hasDesignationLeisure()
            }

            modifierOR(priorities.disable) {
                comment = "Disallow for various conditions"
                disableIfNoNeedForJobs()
                controller {
                    missingIncomeConsumerGoods = true
                    missingIncomeEnergy = true
                }
                hasDesignationLeisure(false)
            }
        }
    }

}
