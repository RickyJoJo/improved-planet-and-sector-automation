package ipa.sectorfocus.common

import ipa.PlanetConstants
import ipa.PlanetConstants.Districts
import ipa.blocks.logic.Comparators
import ipa.blocks.sector.SectorFocus

fun SectorFocus.addCommonHousing() {

    // housing districts
    for (district in Districts.Housing) {
        addDistrict(district, priorities.base) {

            modifierOR(priorities.urgent) {
                comment = "Increased priority when almost out of housing"
                freeHousing(Comparators.LTEQ, 1)
                AND {
                    comment = "or when out of amenities for gestalt"
                    owner { isGestalt = true }
                    freeAmenities(Comparators.LTEQ, 1)
                }
            }

            modifierOR(priorities.crisis / priorities.urgent) {
                comment = "Critical priority when out of housing"
                freeHousing(Comparators.LTEQ, 0)
                AND {
                    comment = "or when out of amenities for gestalt"
                    owner { isGestalt = true }
                    freeAmenities(Comparators.LTEQ, 0)
                }
            }

            modifierOR(priorities.disable) {
                comment = "Disallow when not needed"

                AND {
                    comment = "For gestalt consider free amenities in addition to housing"
                    owner { isGestalt = true }
                    freeHousing(Comparators.GT, 2)
                    freeAmenities(Comparators.GTEQ, 1)
                }

                AND {
                    comment = "Disallow for non-gestalt based on housing alone"
                    owner { isGestalt = false }
                    freeHousing(Comparators.GT, 2)
                }
            }
        }
    }

    // housing buildings T1
    for (building in PlanetConstants.HousingBuildingsT1) {
        addBuilding(building, priorities.base) {

            modifier(priorities.urgent) {
                comment = "Increased priority when almost out of housing"
                freeHousing(Comparators.LTEQ, 1)
            }

            modifier(priorities.crisis / priorities.urgent) {
                comment = "Critical priority when out of housing"
                freeHousing(Comparators.LTEQ, 0)
            }

            modifierOR(priorities.disable) {
                comment = "Disallow when not needed"
                freeDistrictSlots(Comparators.GT, 0)
                freeHousing(Comparators.GT, 2)
            }
        }
    }

}
