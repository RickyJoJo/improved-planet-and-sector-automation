package ipa.sectorfocus.common

import ipa.blocks.sector.SectorFocus

fun SectorFocus.addCommonStrategicNatural() {

    addBuilding("building_mote_harvesters", priorities.naturalStrategic) {
        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingIncomeVolatileMotes = true
            }
        }
        modifier(priorities.disable) {
            comment = "Disallow if to many free jobs"
            disableIfNoNeedForJobs()
        }
    }
    addBuilding("building_gas_extractors", priorities.naturalStrategic) {
        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingIncomeExoticGases = true
            }
        }
        modifier(priorities.disable) {
            comment = "Disallow if to many free jobs"
            disableIfNoNeedForJobs()
        }
    }

    addBuilding("building_crystal_mines", priorities.naturalStrategic) {
        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingIncomeRareCrystals = true
            }
        }
        modifier(priorities.disable) {
            comment = "Disallow if to many free jobs"
            disableIfNoNeedForJobs()
        }
    }

}
