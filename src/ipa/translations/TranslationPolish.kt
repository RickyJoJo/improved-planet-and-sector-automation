package ipa.translations

import ipa.blocks.TranslationFile

class TranslationPolish : TranslationFile("localisation/polish/ipa_l_polish.yml") {

    init {
        language = "l_polish"
        TranslationEnglish.addBase(this)
    }

}
