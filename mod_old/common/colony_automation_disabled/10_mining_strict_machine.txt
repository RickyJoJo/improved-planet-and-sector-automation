automate_mining_planet_strict_machine = {
	available = {
		has_designation = col_mining_strict
		free_jobs < 3
		owner = { has_authority = auth_machine_intelligence	}
	}

	prio_districts = {
		district_mining
	}

	buildings = {
		1 = {
			building = building_deployment_post
			available = {
				always = yes
			}
		}

		2 = {
			building = building_mineral_purification_plant
			available = {
				owner = {
					mineral_purification_plant_upkeep_affordable = yes
				}
			}
		}

		3 = {
			building = building_uplink_node
			available = {
				owner = {
					uplink_node_upkeep_affordable = yes
				}
			}
		}

	}
}