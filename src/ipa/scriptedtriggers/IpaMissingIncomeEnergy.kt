package ipa.scriptedtriggers

import ipa.Policies
import ipa.Resources
import ipa.Variables
import ipa.blocks.triggers.CountryScriptedTrigger

class IpaMissingIncomeEnergy : CountryScriptedTrigger("ipa_missing_income_energy") {

    init {
        addMinimumIncome(Resources.Energy, Variables.IncomeEnergyMinimum, Policies.incomeSteps)
    }

}
