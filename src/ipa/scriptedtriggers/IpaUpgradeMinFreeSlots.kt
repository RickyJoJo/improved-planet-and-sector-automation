package ipa.scriptedtriggers

import ipa.Variables
import ipa.blocks.logic.Comparators
import ipa.blocks.triggers.PlanetaryScriptedTrigger

class IpaUpgradeMinFreeSlots : PlanetaryScriptedTrigger("ipa_upgrade_min_free_slots") {

    init {
        OR {
            // always true if no more free slots are available
            freeBuildingSlots(Comparators.EQ, 0)

            for (i in 1..10) {
                AND {
                    owner {
                        checkVariable(Variables.OptionsUpgradeMinFreeSlots, Comparators.EQ, i)
                    }
                    freeBuildingSlots(Comparators.LTEQ, i)
                }
            }
        }
    }

}

