package ipa.scriptedtriggers

import ipa.PlanetConstants
import ipa.blocks.triggers.PlanetaryScriptedTrigger

class IpaHasDesignationCapital : PlanetaryScriptedTrigger("ipa_has_designation_capital") {
    init {
        OR {
            PlanetConstants.Designations.Capital.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationGenerator : PlanetaryScriptedTrigger("ipa_has_designation_generator") {
    init {
        OR {
            PlanetConstants.Designations.Generator.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationMining : PlanetaryScriptedTrigger("ipa_has_designation_mining") {
    init {
        OR {
            PlanetConstants.Designations.Mining.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationFarming : PlanetaryScriptedTrigger("ipa_has_designation_farming") {
    init {
        OR {
            PlanetConstants.Designations.Farming.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationResearch : PlanetaryScriptedTrigger("ipa_has_designation_research") {
    init {
        OR {
            PlanetConstants.Designations.Research.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationLeisure : PlanetaryScriptedTrigger("ipa_has_designation_leisure") {
    init {
        OR {
            PlanetConstants.Designations.Leisure.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationCommercial : PlanetaryScriptedTrigger("ipa_has_designation_commercial") {
    init {
        OR {
            PlanetConstants.Designations.Commercial.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationUrban : PlanetaryScriptedTrigger("ipa_has_designation_urban") {
    init {
        OR {
            PlanetConstants.Designations.Urban.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationFactory : PlanetaryScriptedTrigger("ipa_has_designation_factory") {
    init {
        OR {
            PlanetConstants.Designations.Factory.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationRefinery : PlanetaryScriptedTrigger("ipa_has_designation_refinery") {
    init {
        OR {
            PlanetConstants.Designations.Refinery.forEach(::hasDesignation)
        }
    }
}


class IpaHasDesignationForge : PlanetaryScriptedTrigger("ipa_has_designation_forge") {
    init {
        OR {
            PlanetConstants.Designations.Forge.forEach(::hasDesignation)
        }
    }
}


class IpaHasDesignationFortress : PlanetaryScriptedTrigger("ipa_has_designation_fortress") {
    init {
        OR {
            PlanetConstants.Designations.Fortress.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationRural : PlanetaryScriptedTrigger("ipa_has_designation_rural") {
    init {
        OR {
            PlanetConstants.Designations.Rural.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationPenal : PlanetaryScriptedTrigger("ipa_has_designation_penal") {
    init {
        OR {
            PlanetConstants.Designations.Penal.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationResort : PlanetaryScriptedTrigger("ipa_has_designation_resort") {
    init {
        OR {
            PlanetConstants.Designations.Resort.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationMix : PlanetaryScriptedTrigger("ipa_has_designation_mix") {
    init {
        OR {
            PlanetConstants.Designations.Mix.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationBureau : PlanetaryScriptedTrigger("ipa_has_designation_bureau") {
    init {
        OR {
            PlanetConstants.Designations.Bureau.forEach(::hasDesignation)
        }
    }
}

