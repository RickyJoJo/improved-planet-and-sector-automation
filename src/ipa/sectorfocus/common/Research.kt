package ipa.sectorfocus.common

import ipa.blocks.logic.Comparators
import ipa.blocks.sector.SectorFocus

fun SectorFocus.addCommonResearch(
    onlyOnMatchingDesignation: Boolean = true,
    notOnSpecializedWorlds: Boolean = true
) {

    // research enhancers
    for (building in arrayOf("building_supercomputer", "building_institute")) {
        addBuilding(building, priorities.enhancers) {
            modifierOR(priorities.disable) {
                comment = "Disallow when not research designation or to many free jobs"
                disableIfNoNeedForJobs()

                if (onlyOnMatchingDesignation) {
                    NOR {
                        comment = "exclude non-research worlds"
                        hasDesignationResearch()
                        hasDesignationCapital()
                    }
                } else if (notOnSpecializedWorlds) {
                    OR {
                        comment = "exclude specific production worlds"
                        hasDesignationFactory()
                        hasDesignationRefinery()
                        hasDesignationForge()
                    }
                }

                controller {
                    missingIncomeEnergy = true
                }

                AND {
                    comment = "disallow if no research buildings"
                    numBuildings("building_research_lab_1", Comparators.EQ, 0)
                    numBuildings("building_research_lab_2", Comparators.EQ, 0)
                    numBuildings("building_research_lab_3", Comparators.EQ, 0)
                }

                addResearchRequirementsGestaltAltBlock()
            }
        }
    }

    addBuilding("building_research_lab_1", priorities.research) {
        modifierOR(priorities.disable) {
            comment = "Disallow when: not research designation, to many free jobs or not enough income"
            disableIfNoNeedForJobs()

            if (onlyOnMatchingDesignation) {
                NOR {
                    comment = "exclude non-research worlds"
                    hasDesignationResearch()
                    hasDesignationCapital()
                }
            } else if (notOnSpecializedWorlds) {
                OR {
                    comment = "exclude specific production worlds"
                    hasDesignationFactory()
                    hasDesignationRefinery()
                    hasDesignationForge()
                }
            }

            controller {
                missingIncomeEnergy = true
            }

            addResearchRequirementsGestaltAltBlock()
        }
    }

    addBuilding("building_research_lab_2", priorities.research + 1) {
        modifierOR(priorities.disable) {
            comment = "Disallow when: not research designation, to many free jobs or not enough income"
            disableIfNoNeedForJobs()

            if (onlyOnMatchingDesignation) {
                NOR {
                    comment = "exclude non-research worlds"
                    hasDesignationResearch()
                    hasDesignationCapital()
                }
            } else if (notOnSpecializedWorlds) {
                OR {
                    comment = "exclude specific production worlds"
                    hasDesignationFactory()
                    hasDesignationRefinery()
                    hasDesignationForge()
                }
            }

            controller {
                missingIncomeEnergy = true
                missingIncomeExoticGases = true
                upgradeMinFreeBuildingSlots()
            }

            addResearchRequirementsGestaltAltBlock()
        }
    }

    addBuilding("building_research_lab_3", priorities.research + 2) {
        modifierOR(priorities.disable) {
            comment = "Disallow when: not research designation, to many free jobs or not enough income"
            disableIfNoNeedForJobs()

            if (onlyOnMatchingDesignation) {
                NOR {
                    comment = "exclude non-research worlds"
                    hasDesignationResearch()
                    hasDesignationCapital()
                }
            } else if (notOnSpecializedWorlds) {
                OR {
                    comment = "exclude specific production worlds"
                    hasDesignationFactory()
                    hasDesignationRefinery()
                    hasDesignationForge()
                }
            }

            controller {
                missingIncomeEnergy = true
                missingIncomeExoticGases = true

                upgradeMinFreeBuildingSlots()
            }
            addResearchRequirementsGestaltAltBlock()
        }
    }

}

