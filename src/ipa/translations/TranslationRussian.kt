package ipa.translations

import ipa.blocks.TranslationFile

class TranslationRussian : TranslationFile("localisation/russian/ipa_l_russian.yml") {

    init {
        language = "l_russian"
        TranslationEnglish.addBase(this)
    }

}
