package ipa.scriptedtriggers

import ipa.Policies
import ipa.Resources
import ipa.Variables
import ipa.blocks.triggers.CountryScriptedTrigger

class IpaMissingIncomeMinerals : CountryScriptedTrigger("ipa_missing_income_minerals") {

    init {
        addMinimumIncome(Resources.Minerals, Variables.IncomeMineralsMinimum, Policies.incomeSteps)
    }

}
