
automate_fortress_hive_planet = {
	available = {
		has_designation = col_fortress
		owner = { has_authority = auth_machine_intelligence	}
		free_jobs < 3
	}

	prio_districts = {
		district_hive
	}

	buildings = {
		1 = {
			building = building_hive_capital
			available = {
				always = yes
			}
		}

	#	2 = {
	#		building = building_spawning_pool
	#		available = {
	#			AND = {
	#				owner = { spawning_pool_upkeep_affordable = yes }
	#				owner = { has_policy_flag = ipa_allow_growth_buildings }
	#			}
	#		}
	#	}

		3 = {
			building = building_stronghold
			available = {
				owner = {
					stronghold_upkeep_affordable = yes
				}
			}
		}

		4 = {
			building = building_hive_node
			available = {
				owner = {
					hive_node_upkeep_affordable = yes
				}
			}
		}

		5 = {
			building = building_stronghold
			available = {
				owner = {
					stronghold_upkeep_affordable = yes
				}
			}
		}

		6 = {
			building = building_chemical_plant
			available = {
				owner = {
					chemical_plant_upkeep_affordable = yes
				}
			}
		}

		7 = {
			building = building_stronghold
			available = {
				owner = {
					stronghold_upkeep_affordable = yes
				}
			}
		}

		8 = {
			building = building_stronghold
			available = {
				owner = {
					stronghold_upkeep_affordable = yes
				}
			}
		}

		9 = {
			building = building_chemical_plant
			available = {
				owner = {
					chemical_plant_upkeep_affordable = yes
				}
			}
		}

		10 = {
			building = building_stronghold
			available = {
				owner = {
					stronghold_upkeep_affordable = yes
				}
			}
		}

		11 = {
			building = building_stronghold
			available = {
				owner = {
					stronghold_upkeep_affordable = yes
				}
			}
		}

		12 = {
			building = building_planetary_shield_generator
			available = {
				owner = {
					planetary_shield_generator_upkeep_affordable = yes
				}
			}
		}
	}
}