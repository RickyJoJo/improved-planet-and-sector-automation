package ipa.blocks.sector

import ipa.blocks.TemplateRender
import ipa.blocks.logic.PlanetaryConditionalBlock

class SectorWeightModifier(val factor: Double) : PlanetaryConditionalBlock("modifier") {

    override fun renderBeforeChildren(render: TemplateRender) {
        render.add("factor = $factor")
        super.renderBeforeChildren(render)
    }

}
