package ipa.sectorfocus.common

import ipa.Variables
import ipa.blocks.logic.Comparators
import ipa.blocks.sector.SectorFocus

fun SectorFocus.addCommonMilitaryBuildings() {

    addBuilding("building_stronghold", priorities.default) {
        modifierOR(priorities.disable) {
            hasDesignationFortress(false) // forbidden on non-fortress worlds
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
            }
        }
    }

    addBuilding("building_fortress", priorities.defaultReduced) {
        modifierOR(priorities.disable) {
            disableIfNoNeedForJobs()
            upgradeMinFreeBuildingSlots()
            controller {
                missingIncomeVolatileMotes = true
            }
            owner {
                checkVariable(Variables.OptionsAllowUpgradeStronghold, Comparators.EQ, 0)
            }
        }
    }

}
