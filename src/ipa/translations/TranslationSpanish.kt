package ipa.translations

import ipa.blocks.TranslationFile

class TranslationSpanish : TranslationFile("localisation/spanish/ipa_l_spanish.yml") {

    init {
        language = "l_spanish"
        TranslationEnglish.addBase(this)
    }

}
