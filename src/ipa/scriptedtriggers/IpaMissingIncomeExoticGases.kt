package ipa.scriptedtriggers

import ipa.Policies
import ipa.Resources
import ipa.Variables
import ipa.blocks.triggers.CountryScriptedTrigger

class IpaMissingIncomeExoticGases : CountryScriptedTrigger("ipa_missing_income_exotic_gases") {

    init {
        addMinimumIncome(Resources.ExoticGases, Variables.IncomeStrategicMinimum, Policies.incomeStepsStrategic)
    }

}
