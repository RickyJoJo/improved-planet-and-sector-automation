package ipa.blocks.logic

import ipa.Variables
import ipa.blocks.TemplateBlock
import ipa.blocks.TemplateRender

open class EffectBlock(blockKey: String?) : TemplateBlock(blockKey) {

    val actions: MutableList<String> = mutableListOf()

    override fun renderBeforeChildren(render: TemplateRender) {
        if (actions.isNotEmpty()) {
            for (action in actions) {
                render.add(action)
            }
        }
    }

    fun setGlobalFlag(name: String) {
        actions.add("set_global_flag = $name")
    }

    fun removeGlobalFlag(name: String) {
        actions.add("remove_global_flag = $name")
    }

    fun countryEvent(id: String) {
        actions.add("country_event = { id = $id }")
    }

    fun setVariable(name: String, value: Int) {
        actions.add("set_variable = { which = $name value = $value }")
    }

    fun setVariable(name: Variables, value: Int) {
        setVariable(name.key, value)
    }

}
