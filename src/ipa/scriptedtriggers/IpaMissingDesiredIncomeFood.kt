package ipa.scriptedtriggers

import ipa.Policies
import ipa.Resources
import ipa.Variables
import ipa.blocks.triggers.CountryScriptedTrigger

class IpaMissingDesiredIncomeFood : CountryScriptedTrigger("ipa_missing_desired_income_food") {

    init {
        addDesiredIncome(Resources.Food, Variables.IncomeFoodDesired, Policies.incomeSteps)
    }

}
