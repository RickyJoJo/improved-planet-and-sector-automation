namespace = ipa_settings_options

# CountryEvent: ipa_settings_options.1
country_event = {
	id = ipa_settings_options.1
	title = ipa_settings_options.1.title
	desc = ipa_settings_options.1.desc
	hide_window = no
	is_triggered_only = yes

	# EventOption: ipa_settings_options.1.allow_growth_buildings
	option = {
		name = ipa_settings_options.1.allow_growth_buildings

		hidden_effect = {
			country_event = { id = ipa_settings_options_allow_growth_buildings.1 }
		}

	}

	# EventOption: ipa_settings_options.1.upgrade_min_free_slots
	option = {
		name = ipa_settings_options.1.upgrade_min_free_slots

		hidden_effect = {
			country_event = { id = ipa_settings_options_upgrade_min_free_slots.1 }
		}

	}

	# EventOption: ipa_settings_options.1.admin_cap
	option = {
		name = ipa_settings_options.1.admin_cap

		hidden_effect = {
			country_event = { id = ipa_settings_options_admin_cap.1 }
		}

	}

	# EventOption: ipa_settings_options.1.ipa_options_allow_upgrade_strongholds_yes
	option = {
		name = ipa_settings_options.1.ipa_options_allow_upgrade_strongholds_yes

		trigger = {
			check_variable = { which = ipa_options_allow_upgrade_strongholds value = 1 }
		}

		hidden_effect = {
			set_variable = { which = ipa_options_allow_upgrade_strongholds value = 0 }
			country_event = { id = ipa_settings_options.1 }
		}

	}

	# EventOption: ipa_settings_options.1.ipa_options_allow_upgrade_strongholds_no
	option = {
		name = ipa_settings_options.1.ipa_options_allow_upgrade_strongholds_no

		trigger = {
			check_variable = { which = ipa_options_allow_upgrade_strongholds value = 0 }
		}

		hidden_effect = {
			set_variable = { which = ipa_options_allow_upgrade_strongholds value = 1 }
			country_event = { id = ipa_settings_options.1 }
		}

	}

	# EventOption: ipa_settings_options.1.ipa_options_allow_agri_farm_buildings_yes
	option = {
		name = ipa_settings_options.1.ipa_options_allow_agri_farm_buildings_yes

		trigger = {
			check_variable = { which = ipa_options_allow_agri_farm_buildings value = 1 }
		}

		hidden_effect = {
			set_variable = { which = ipa_options_allow_agri_farm_buildings value = 0 }
			country_event = { id = ipa_settings_options.1 }
		}

	}

	# EventOption: ipa_settings_options.1.ipa_options_allow_agri_farm_buildings_no
	option = {
		name = ipa_settings_options.1.ipa_options_allow_agri_farm_buildings_no

		trigger = {
			check_variable = { which = ipa_options_allow_agri_farm_buildings value = 0 }
		}

		hidden_effect = {
			set_variable = { which = ipa_options_allow_agri_farm_buildings value = 1 }
			country_event = { id = ipa_settings_options.1 }
		}

	}

	# EventOption: ipa_settings_options.1.ipa_options_force_show_income_options_yes
	option = {
		name = ipa_settings_options.1.ipa_options_force_show_income_options_yes

		trigger = {
			check_variable = { which = ipa_options_force_show_income_options value = 1 }
		}

		hidden_effect = {
			set_variable = { which = ipa_options_force_show_income_options value = 0 }
			country_event = { id = ipa_settings_options.1 }
		}

	}

	# EventOption: ipa_settings_options.1.ipa_options_force_show_income_options_no
	option = {
		name = ipa_settings_options.1.ipa_options_force_show_income_options_no

		trigger = {
			check_variable = { which = ipa_options_force_show_income_options value = 0 }
		}

		hidden_effect = {
			set_variable = { which = ipa_options_force_show_income_options value = 1 }
			country_event = { id = ipa_settings_options.1 }
		}

	}

	# EventOption: ipa_settings_options.1.ipa_options_disallow_unity_yes
	option = {
		name = ipa_settings_options.1.ipa_options_disallow_unity_yes

		trigger = {
			check_variable = { which = ipa_options_disallow_unity value = 1 }
		}

		hidden_effect = {
			set_variable = { which = ipa_options_disallow_unity value = 0 }
			country_event = { id = ipa_settings_options.1 }
		}

	}

	# EventOption: ipa_settings_options.1.ipa_options_disallow_unity_no
	option = {
		name = ipa_settings_options.1.ipa_options_disallow_unity_no

		trigger = {
			check_variable = { which = ipa_options_disallow_unity value = 0 }
		}

		hidden_effect = {
			set_variable = { which = ipa_options_disallow_unity value = 1 }
			country_event = { id = ipa_settings_options.1 }
		}

	}

	# EventOption: ipa_settings_options.1.back
	option = {
		name = ipa_settings_options.1.back

		hidden_effect = {
			country_event = { id = ipa_settings_main.1 }
		}

	}

}

