package ipa.scriptedtriggers

import ipa.Policies
import ipa.Resources
import ipa.Variables
import ipa.blocks.triggers.CountryScriptedTrigger

class IpaMissingIncomeVolatileMotes : CountryScriptedTrigger("ipa_missing_income_volatile_motes") {

    init {
        addMinimumIncome(Resources.VolatileMotes, Variables.IncomeStrategicMinimum, Policies.incomeStepsStrategic)
    }

}
