package ipa.sectorfocus.common

import ipa.PlanetConstants
import ipa.blocks.logic.Comparators
import ipa.blocks.sector.SectorFocus

fun SectorFocus.addCommonCrime() {

    // anti-crime T1
    for (building in PlanetConstants.CrimeBuildingsT1) {
        addBuilding(building, 1.1) {

            modifier(priorities.prebuild) {
                comment = "Pre-build when approaching threshold"
                planetCrime(Comparators.GTEQ, 8)
            }

            modifierAND(priorities.urgent / priorities.prebuild) {
                comment = "Increase priority when approaching threshold"
                planetCrime(Comparators.GT, 8)
                planetCrime(Comparators.LT, 15)
            }

            modifier(priorities.crisis / priorities.urgent) {
                comment = "Increase to crisis status if this gets out of hand"
                planetCrime(Comparators.GTEQ, 15)
            }

            modifierOR(priorities.disable) {
                comment = "Disallow when no workers are available or no crime exists"
                planetCrime(Comparators.LT, 8)
                freeJobs(Comparators.GT, 3)
            }
        }
    }

}
