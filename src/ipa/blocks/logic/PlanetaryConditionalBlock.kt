package ipa.blocks.logic

import ipa.Sectors
import ipa.blocks.TemplateRender
import ipa.scriptedtriggers.IpaAdminCapAllowed
import ipa.scriptedtriggers.IpaUnityAllowed
import kotlin.reflect.KMutableProperty0

abstract class RawPlanetaryConditionalBlock<T : RawPlanetaryConditionalBlock<T>>(blockKey: String?) :
    ConditionalBlock<T>(blockKey) {

    private var controller: CountryConditionalBlock? = null
    private var owner: CountryConditionalBlock? = null

    protected fun countryChildBlock(
        field: KMutableProperty0<CountryConditionalBlock?>,
        type: String,
        block: CountryConditionalBlock.() -> Unit
    ) {
        var b = field.get()
        if (b == null) {
            b = CountryConditionalBlock(type)
            b.compact = true
            field.set(b)
        }
        b.block()
    }

    fun owner(block: CountryConditionalBlock.() -> Unit) {
        countryChildBlock(::owner, "owner", block)
    }

    fun controller(block: CountryConditionalBlock.() -> Unit) {
        countryChildBlock(::controller, "controller", block)
    }

    fun hasDesignationCapital(flag: Boolean = true) {
        has("ipa_has_designation_capital", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationGenerator(flag: Boolean = true) {
        has("ipa_has_designation_generator", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationMining(flag: Boolean = true) {
        has("ipa_has_designation_mining", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationFarming(flag: Boolean = true) {
        has("ipa_has_designation_farming", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationResearch(flag: Boolean = true) {
        has("ipa_has_designation_research", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationLeisure(flag: Boolean = true) {
        has("ipa_has_designation_leisure", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationCommercial(flag: Boolean = true) {
        has("ipa_has_designation_commercial", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationUrban(flag: Boolean = true) {
        has("ipa_has_designation_urban", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationFactory(flag: Boolean = true) {
        has("ipa_has_designation_factory", Comparators.EQ, flag.toYesNo())
    }


    fun hasDesignationRefinery(flag: Boolean = true) {
        has("ipa_has_designation_refinery", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationForge(flag: Boolean = true) {
        has("ipa_has_designation_forge", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationRural(flag: Boolean = true) {
        has("ipa_has_designation_rural", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationPenal(flag: Boolean = true) {
        has("ipa_has_designation_penal", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationResort(flag: Boolean = true) {
        has("ipa_has_designation_resort", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationMix(flag: Boolean = true) {
        has("ipa_has_designation_mix", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationBureau(flag: Boolean = true) {
        has("ipa_has_designation_bureau", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationFortress(flag: Boolean = true) {
        has("ipa_has_designation_fortress", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignation(designation: String) {
        conditions.add("has_designation = $designation")
    }

    fun freeJobs(cmp: Comparators, value: Int) {
        conditions.add("free_jobs $cmp $value")
    }

    fun disableIfNoNeedForJobs() {
        // conditions.add("has_unemployed_or_servants = no")
        freeJobs(Comparators.GT, Sectors.BuildMoreJobsLimit)
    }

    fun freeAmenities(cmp: Comparators, value: Int) {
        conditions.add("free_amenities $cmp $value")
    }

    fun freeHousing(cmp: Comparators, value: Int) {
        conditions.add("free_housing $cmp $value")
    }

    fun freeDistrictSlots(cmp: Comparators, value: Int) {
        conditions.add("free_district_slots $cmp $value")
    }

    fun freeBuildingSlots(cmp: Comparators, value: Int) {
        conditions.add("free_building_slots $cmp $value")
    }

    fun upgradeMinFreeBuildingSlots(flag: Boolean = true) {
        conditions.add("ipa_upgrade_min_free_slots = ${flag.toYesNo()}")
    }

    fun adminCapAllowed(flag: Boolean = true) {
        conditions.add("${IpaAdminCapAllowed.Name} = ${flag.toYesNo()}")
    }

    fun unityAllowed(flag: Boolean = true) {
        conditions.add("${IpaUnityAllowed.Name} = ${flag.toYesNo()}")
    }

    fun planetCrime(cmp: Comparators, value: Int) {
        conditions.add("planet_crime $cmp $value")
    }

    fun numFreeDistricts(type: String, cmp: Comparators, value: Int) {
        conditions.add("num_free_districts = { type = $type value $cmp $value }")
    }

    fun numDistricts(type: String, cmp: Comparators, value: Int) {
        conditions.add("num_districts = { type = $type value $cmp $value }")
    }

    fun numBuildings(type: String, cmp: Comparators, value: Int) {
        conditions.add("num_buildings = { type = $type value $cmp $value }")
    }

    fun addResearchRequirementsGestaltAltBlock() {
        AND {
            comment = "non-gestalt empires require consumer goods for research"
            owner {
                isGestalt = false
            }
            controller {
                missingIncomeConsumerGoods = true
            }
        }

        AND {
            comment = "gestalt empires require minerals for research"
            owner {
                isGestalt = true
            }
            controller {
                missingIncomeMinerals = true
            }
        }
    }

    fun addAdminCapRequirementsGestaltAltBlock() {
        AND {
            comment = "non-gestalt empires require consumer goods for admin cap"
            owner {
                isGestalt = false
            }
            controller {
                missingIncomeConsumerGoods = true
            }
        }
    }

    override fun hasConditions(): Boolean {
        return super.hasConditions()
            || owner?.hasConditions() == true
            || controller?.hasConditions() == true
    }

    override fun renderConditions(render: TemplateRender) {
        super.renderConditions(render)

        if (owner?.hasConditions() == true || owner?.children?.isNotEmpty() == true) {
            render.indentedBlock("owner =") {
                render.indentedBlock("${childBlockKey()} =") {
                    if (!owner!!.comment.isNullOrBlank())
                        render.add("# ${owner!!.comment}")
                    owner!!.renderConditions(render)
                    for (child in owner!!.children) {
                        child.render(render)
                    }
                }
            }
        }

        if (controller?.hasConditions() == true || controller?.children?.isNotEmpty() == true) {
            render.indentedBlock("controller =") {
                render.indentedBlock("${childBlockKey()} =") {
                    if (!controller!!.comment.isNullOrBlank())
                        render.add("# ${controller!!.comment}")
                    controller!!.renderConditions(render)
                    for (child in controller!!.children) {
                        child.render(render)
                    }
                }
            }
        }
    }

}


open class PlanetaryConditionalBlock(blockKey: String?) :
    RawPlanetaryConditionalBlock<PlanetaryConditionalBlock>(blockKey) {
    override fun newChildBlock(key: String): PlanetaryConditionalBlock {
        return PlanetaryConditionalBlock(key)
    }
}
