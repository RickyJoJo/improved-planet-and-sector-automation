package ipa.sectorfocus.common

import ipa.Variables
import ipa.blocks.logic.Comparators
import ipa.blocks.sector.SectorFocus

fun SectorFocus.addCommonHydroponicsFarm() {

    addBuilding("building_hydroponics_farm", priorities.defaultReduced) {
        modifierOR(priorities.disable) {
            hasDesignationFarming(false)
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
            }
            owner {
                checkVariable(Variables.OptionsAllowAgriFarmBuildings, Comparators.EQ, 0)
            }
        }
    }

}
