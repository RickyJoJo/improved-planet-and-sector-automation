package ipa.blocks.sector

import ipa.blocks.TemplateBlock
import ipa.blocks.TemplateRender
import ipa.blocks.logic.toYesNo

open class SectorFocus(val focusName: String, block: (SectorFocus.() -> Unit)? = null) : TemplateBlock(focusName) {

    var aiWeight: Int? = 0
    var clearBlockers = true
    var hidden: Boolean? = null

    val priorities: SectorPriorities = SectorPriorities()

    init {
        comment = "Sector focus: $focusName"

        if (block != null) this.block()
    }

    override fun renderBeforeChildren(render: TemplateRender) {
        render.add()
        if (aiWeight != null) {
            render.indentedBlock("ai_weight =") {
                render.add("weight = $aiWeight")
            }
        }

        render.add()
        render.add("clear_blockers = ${clearBlockers.toYesNo()}")
        hidden?.let {
            render.add("hidden = ${it.toYesNo()}")
        }
    }

    fun addBuilding(key: String, block: (SectorBuilding.() -> Unit)? = null): SectorBuilding {
        val building = SectorBuilding(key)
        if (block != null) building.block()
        children.add(building)
        return building
    }

    fun addBuilding(key: String, weight: Double, block: (SectorWeight.() -> Unit)): SectorBuilding {
        val building = SectorBuilding(key)
        building.weight(weight, block)
        children.add(building)
        return building
    }

    fun addDistrict(key: String, block: (SectorDistrict.() -> Unit)? = null): SectorDistrict {
        val district = SectorDistrict(key)
        if (block != null) district.block()
        children.add(district)
        return district
    }

    fun addDistrict(key: String, weight: Double, block: (SectorWeight.() -> Unit)): SectorDistrict {
        val district = SectorDistrict(key)
        district.weight(weight, block)
        children.add(district)
        return district
    }

}
